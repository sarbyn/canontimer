/**********************************************/
/* Arduino CanonTimer                         */
/* Version 0.1                                */
/* Author : Sarbyn                            */
/* Website : www.sarbyn.com                   */
/**********************************************/

// 7 segments display
#define LED_ANODE_PIN    2

#define LED_A_PIN        9  
#define LED_B_PIN        10
#define LED_C_PIN        5
#define LED_D_PIN        4
#define LED_E_PIN        3
#define LED_F_PIN        8
#define LED_G_PIN        7

#define LED_DOT_PIN      6

// program selector button
#define SELECTOR_PIN      11

// start-stop button
#define START_STOP_PIN    12

// output pin
#define OUTPUT_PIN        13

#define DEBUG false

int program_number;
boolean running;

long lastAction = 0;
long delays[] = {5000, 10000, 15000, 30000, 45000, 60000, 90000, 120000, 180000, 300000};

void setup() {
  Serial.begin(9600);
  
  if (DEBUG)
    Serial.println("Start setup!");
  
  // setup LCD pins
  pinMode(LED_ANODE_PIN , OUTPUT);
  pinMode(LED_A_PIN , OUTPUT);
  pinMode(LED_B_PIN , OUTPUT);
  pinMode(LED_C_PIN , OUTPUT);
  pinMode(LED_D_PIN , OUTPUT);
  pinMode(LED_E_PIN , OUTPUT);
  pinMode(LED_F_PIN , OUTPUT);
  pinMode(LED_G_PIN , OUTPUT);
  pinMode(LED_DOT_PIN , OUTPUT);
  
  // setup selector pin
  pinMode(SELECTOR_PIN, INPUT);
  
  // setup start-stop button
  pinMode(START_STOP_PIN, INPUT);
  
  // setup output pin
  pinMode(OUTPUT_PIN, OUTPUT);
  
  //blink test
  doStartupBlinkTest();
  
  program_number = 0;
  
  running = false;
  if (DEBUG)  
    Serial.println("Setup done!");
}

void loop() {
  printNumber(program_number);
  
  if (digitalRead(SELECTOR_PIN) == HIGH) {
    if (DEBUG)
      Serial.println("Selector pressed");
    if (!running) {
      delay(200);
      program_number++;
    } else {
      doStartupBlinkTest();
    }
  }
    
  if (digitalRead(START_STOP_PIN) == HIGH) {
    if (DEBUG)
      Serial.println("Start pressed");
    delay(200);
    running = !running;
  }
    
  if (program_number == 10)
    program_number = 0;
    
  if (running) {
    blinkDot();
    checkAndDoAction();
  } else {
    digitalWrite(LED_DOT_PIN, HIGH);
    lastAction = 0;
  }
}

void checkAndDoAction() {
  if (lastAction == 0) {
    doAction();
  }

  long currentDelay = millis() - lastAction;
  
  if (DEBUG) {
    Serial.print("Program delay is ");
    Serial.print(delays[program_number]);
    Serial.print(" and current program_number is ");
    Serial.println(program_number);
  }
  
  if (currentDelay > delays[program_number]) {
    doAction();
  } 
}

void doAction() {
   if (DEBUG)
     Serial.print("Do action - timestamp is ");
   lastAction = millis();
   Serial.println(lastAction);
   digitalWrite(OUTPUT_PIN, HIGH);
   delay(250);
   digitalWrite(OUTPUT_PIN, LOW);
   
   if (DEBUG)
     Serial.println("Done!");
}

void doStartupBlinkTest () {
  for (int i = 0; i != 3; i++) {
    digitalWrite(LED_ANODE_PIN, HIGH);
    delay(150);
    digitalWrite(LED_ANODE_PIN, LOW);
    delay(150);
  }
}

long lastTimeBlinkAction = 0;
int blinkPinStatus;

void blinkDot() {
  if (lastTimeBlinkAction == 0) {
    if (DEBUG)
      Serial.println("Init blink");
    digitalWrite(LED_DOT_PIN, LOW);
    lastTimeBlinkAction = millis();
    blinkPinStatus = LOW;
  }
  
  if ((millis() - lastTimeBlinkAction) > 500) {
    if (blinkPinStatus == LOW) {
      digitalWrite(LED_DOT_PIN, HIGH);
      if (DEBUG)
        Serial.println("Blink if ***** HIGH");
      blinkPinStatus = HIGH;
    } else {
      digitalWrite(LED_DOT_PIN, LOW);
      blinkPinStatus = LOW;
      if (DEBUG)
        Serial.println("Blink if ***** LOW");
    }
    lastTimeBlinkAction = millis();
  }
}

void printNumber(int number) {
  // reset all the pins
  for (int i = 2; i != 11; i++) {
    if (i != LED_DOT_PIN)
      digitalWrite(i, HIGH);
  }
  
  switch (number) {
  case 0:
    digitalWrite(LED_A_PIN, LOW);
    digitalWrite(LED_F_PIN, LOW);
    digitalWrite(LED_B_PIN, LOW);
    digitalWrite(LED_C_PIN, LOW);
    digitalWrite(LED_E_PIN, LOW);
    digitalWrite(LED_D_PIN, LOW);
  case 1:
    digitalWrite(LED_B_PIN, LOW);
    digitalWrite(LED_C_PIN, LOW);
    break;
  case 2:
    digitalWrite(LED_A_PIN, LOW);
    digitalWrite(LED_B_PIN, LOW);
    digitalWrite(LED_G_PIN, LOW);
    digitalWrite(LED_E_PIN, LOW);
    digitalWrite(LED_D_PIN, LOW);
    break;
  case 3:
    digitalWrite(LED_A_PIN, LOW);
    digitalWrite(LED_B_PIN, LOW);
    digitalWrite(LED_G_PIN, LOW);
    digitalWrite(LED_C_PIN, LOW);
    digitalWrite(LED_D_PIN, LOW);
    break;
  case 4:
    digitalWrite(LED_F_PIN, LOW);
    digitalWrite(LED_G_PIN, LOW);
    digitalWrite(LED_B_PIN, LOW);
    digitalWrite(LED_C_PIN, LOW);
    break;
  case 5:
    digitalWrite(LED_A_PIN, LOW);
    digitalWrite(LED_F_PIN, LOW);
    digitalWrite(LED_G_PIN, LOW);
    digitalWrite(LED_C_PIN, LOW);
    digitalWrite(LED_D_PIN, LOW);
    break;
  case 6:
    digitalWrite(LED_F_PIN, LOW);
    digitalWrite(LED_G_PIN, LOW);
    digitalWrite(LED_E_PIN, LOW);
    digitalWrite(LED_D_PIN, LOW);
    digitalWrite(LED_C_PIN, LOW);
    break;
  case 7:
    digitalWrite(LED_A_PIN, LOW);
    digitalWrite(LED_B_PIN, LOW);
    digitalWrite(LED_C_PIN, LOW);
    break;
  case 8:
    digitalWrite(LED_A_PIN, LOW);
    digitalWrite(LED_B_PIN, LOW);
    digitalWrite(LED_F_PIN, LOW);
    digitalWrite(LED_G_PIN, LOW);
    digitalWrite(LED_E_PIN, LOW);
    digitalWrite(LED_D_PIN, LOW);
    digitalWrite(LED_C_PIN, LOW);
    break;
  case 9:
    digitalWrite(LED_A_PIN, LOW);
    digitalWrite(LED_F_PIN, LOW);
    digitalWrite(LED_B_PIN, LOW);
    digitalWrite(LED_G_PIN, LOW);
    digitalWrite(LED_C_PIN, LOW);
    break;
  default:  
    break;
  }
  digitalWrite(LED_ANODE_PIN, HIGH);
}

